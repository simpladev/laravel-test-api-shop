<?php

namespace App\Helpers;

use Exception;
use Intervention\Image\Image;
use Intervention\Image\Constraint;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as ImageFactory;

class Images
{
    /**
     * @param string $originalDisk
     * @param string $resizeDisk
     * @param string $directory
     * @param string $filename
     * @param string $extension
     * @param int $width
     * @param int $height
     * @return Image
     * @throws Exception
     */
    public function resize(
        string $originalDisk,
        string $resizeDisk,
        string $directory,
        string $filename,
        string $extension,
        int $width = null,
        int $height = null
    ): Image {
        $originalStorage = Storage::disk($originalDisk);
        $originalImage   = $originalStorage->path($directory . '/' . $filename . '.' . $extension);

        if ($originalStorage->exists($originalImage)) {
            throw new Exception('File not found');
        }

        $resizeStorage = Storage::disk($resizeDisk);
        $resizePath    = $resizeStorage->path($directory . '/');

        if (!$resizeStorage->exists($directory . '/')) {
            $resizeStorage->makeDirectory($directory . '/');
        }

        $newImage = ImageFactory::make($originalImage);

        return $newImage->resize($width, $height, function (Constraint $constraint) {
            $constraint->aspectRatio();
        })->save($resizePath . $this->getResizedFilename($newImage->basename, $width, $height));
    }

    /**
     * @param string $disk
     * @param string $directory
     * @param string $filename
     * @param int $width
     * @param int $height
     * @param bool $set_watermark
     * @return string
     */
    public function getResizedUrl(
        string $disk,
        string $directory,
        string $filename,
        int $width = null,
        int $height = null,
        bool $set_watermark = false
    ) {
        return Storage::disk($disk)->url($directory . '/')
            . $this->getResizedFilename($filename, $width, $height, $set_watermark);
    }

    /**
     * @param string $filename
     * @param int|null $width
     * @param int|null $height
     * @param bool $set_watermark
     * @return string
     */
    public function getResizedFilename(
        string $filename,
        int $width = null,
        int $height = null,
        bool $set_watermark = false
    ) {
        if ('.' != ($dirname = pathinfo($filename, PATHINFO_DIRNAME))) {
            $file = $dirname . '/' . pathinfo($filename, PATHINFO_FILENAME);
        } else {
            $file = pathinfo($filename, PATHINFO_FILENAME);
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        if ($width > 0 || $height > 0) {
            $file_width     = $width > 0 ? $width : '';
            $file_height    = $height > 0 ? $height : '';
            $file_watermark = ($set_watermark ? 'w' : '');

            $resizedFilename = $file . '.' . $file_width . 'x' . $file_height . $file_watermark . '.' . $extension;
        } else {
            $resizedFilename = $file . '.' . ($set_watermark ? 'w.' : '') . $extension;
        }

        return $resizedFilename;
    }

    /**
     * @param string $disk
     * @param string $directory
     * @param string $file
     */
    public function removeOriginal(string $disk, string $directory, string $file)
    {
        Storage::disk($disk)->delete($directory . '/' . $file);
    }

    /**
     * @param string $disk
     * @param string $directory
     * @param string $file
     */
    public function removeResized(string $disk, string $directory, string $file)
    {
        $path = Storage::disk($disk)->path($directory . '/');

        $filename  = pathinfo($file, PATHINFO_FILENAME);
        $extension = pathinfo($file, PATHINFO_EXTENSION);

        File::delete(
            File::glob($path . $filename . '.*x*.' . $extension)
        );
    }
}
