<?php

namespace App\Http\Controllers\Api\Shop\Catalog;

use Exception;
use App\Http\Controllers\Controller;
use App\UseCase\Shop\Brand\BrandService;
use App\Http\Resources\Shop\Catalog\Brand\Base;
use App\Http\Requests\Shop\Catalog\BrandRequest;
use App\Http\Resources\Shop\Catalog\Brand\Detail;
use App\Repositories\Shop\Catalog\BrandRepository;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class BrandController extends Controller
{
    private BrandService    $service;
    private BrandRepository $brands;

    /**
     * @param BrandRepository $brands
     * @param BrandService $service
     */
    public function __construct(BrandRepository $brands, BrandService $service)
    {
        $this->brands  = $brands;
        $this->service = $service;
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        return Base::collection(
            $this->brands->query()->paginate(10)
        );
    }

    /**
     * @param int $id
     * @return Detail
     */
    public function show(int $id): Detail
    {
        return new Detail(
            $this->brands->getOne($id)
        );
    }

    /**
     * @param BrandRequest $request
     * @return Detail
     */
    public function store(BrandRequest $request): Detail
    {
        return new Detail(
            $this->service->create($request->validated())
        );
    }

    /**
     * @param BrandRequest $request
     * @param int $id
     * @return Detail
     */
    public function update(BrandRequest $request, int $id): Detail
    {
        return new Detail(
            $this->service->update($id, $request->validated())
        );
    }

    /**
     * @param int $id
     * @throws Exception
     */
    public function destroy(int $id): void
    {
        $this->service->delete($id);
    }
}
