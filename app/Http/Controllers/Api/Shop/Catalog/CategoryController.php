<?php

namespace App\Http\Controllers\Api\Shop\Catalog;

use Exception;
use App\Http\Controllers\Controller;
use App\UseCase\Shop\Category\CategoryService;
use App\Http\Resources\Shop\Catalog\Category\Base;
use App\Http\Requests\Shop\Catalog\CategoryRequest;
use App\Http\Resources\Shop\Catalog\Category\Detail;
use App\Repositories\Shop\Catalog\CategoryRepository;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CategoryController extends Controller
{
    private CategoryService    $service;
    private CategoryRepository $categories;

    /**
     * @param CategoryRepository $categories
     * @param CategoryService $service
     */
    public function __construct(CategoryRepository $categories, CategoryService $service)
    {
        $this->categories = $categories;
        $this->service    = $service;
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        return Base::collection(
            $this->categories->query()->paginate(10)
        );
    }

    /**
     * @param int $id
     * @return Detail
     */
    public function show(int $id): Detail
    {
        return new Detail(
            $this->categories->getOne($id)
        );
    }

    /**
     * @param CategoryRequest $request
     * @return Detail
     */
    public function store(CategoryRequest $request): Detail
    {
        return new Detail(
            $this->service->create($request->validated())
        );
    }

    /**
     * @param CategoryRequest $request
     * @param int $id
     * @return Detail
     */
    public function update(CategoryRequest $request, int $id): Detail
    {
        return new Detail(
            $this->service->update($id, $request->validated())
        );
    }

    /**
     * @param int $id
     * @throws Exception
     */
    public function destroy(int $id): void
    {
        $this->service->delete($id);
    }
}
