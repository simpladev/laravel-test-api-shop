<?php

namespace App\Http\Controllers\Api\Shop\Catalog;

use Exception;
use App\Http\Controllers\Controller;
use App\UseCase\Shop\Feature\FeatureService;
use App\Http\Resources\Shop\Catalog\Feature\Base;
use App\Http\Requests\Shop\Catalog\FeatureRequest;
use App\Http\Resources\Shop\Catalog\Feature\Detail;
use App\Repositories\Shop\Catalog\FeatureRepository;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class FeatureController extends Controller
{
    private FeatureService    $service;
    private FeatureRepository $features;

    /**
     * @param FeatureRepository $features
     * @param FeatureService $service
     */
    public function __construct(FeatureRepository $features, FeatureService $service)
    {
        $this->features = $features;
        $this->service  = $service;
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        return Base::collection(
            $this->features->query()->paginate(10)
        );
    }

    /**
     * @param int $id
     * @return Detail
     */
    public function show(int $id): Detail
    {
        return new Detail(
            $this->features->getOne($id)
        );
    }

    /**
     * @param FeatureRequest $request
     * @return Detail
     */
    public function store(FeatureRequest $request): Detail
    {
        return new Detail(
            $this->service->create($request->validated())
        );
    }

    /**
     * @param FeatureRequest $request
     * @param int $id
     * @return Detail
     */
    public function update(FeatureRequest $request, int $id): Detail
    {
        return new Detail(
            $this->service->update($id, $request->validated())
        );
    }

    /**
     * @param int $id
     * @throws Exception
     */
    public function destroy(int $id): void
    {
        $this->service->delete($id);
    }
}
