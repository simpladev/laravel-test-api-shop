<?php

namespace App\Http\Controllers\Api\Shop\Catalog;

use Exception;
use App\Http\Controllers\Controller;
use App\UseCase\Shop\Flag\FlagService;
use App\Http\Resources\Shop\Catalog\Flag\Base;
use App\Http\Requests\Shop\Catalog\FlagRequest;
use App\Http\Resources\Shop\Catalog\Flag\Detail;
use App\Repositories\Shop\Catalog\FlagRepository;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class FlagController extends Controller
{
    private FlagService    $service;
    private FlagRepository $flags;

    /**
     * @param FlagRepository $flags
     * @param FlagService $service
     */
    public function __construct(FlagRepository $flags, FlagService $service)
    {
        $this->flags   = $flags;
        $this->service = $service;
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        return Base::collection(
            $this->flags->query()->paginate(10)
        );
    }

    /**
     * @param int $id
     * @return Detail
     */
    public function show(int $id): Detail
    {
        return new Detail(
            $this->flags->getOne($id)
        );
    }

    /**
     * @param FlagRequest $request
     * @return Detail
     */
    public function store(FlagRequest $request): Detail
    {
        return new Detail(
            $this->service->create($request->validated())
        );
    }

    /**
     * @param FlagRequest $request
     * @param int $id
     * @return Detail
     */
    public function update(FlagRequest $request, int $id): Detail
    {
        return new Detail(
            $this->service->update($id, $request->validated())
        );
    }

    /**
     * @param int $id
     * @throws Exception
     */
    public function destroy(int $id): void
    {
        $this->service->delete($id);
    }
}
