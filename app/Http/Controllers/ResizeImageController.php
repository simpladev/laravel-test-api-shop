<?php

namespace App\Http\Controllers;

use App\Models\Shop\Catalog\Category\Category;
use Exception;
use App\Helpers\Images;
use App\Models\Shop\Catalog\Brand\Brand;

class ResizeImageController extends Controller
{
    private static $directories = [
        Brand::IMAGE_DIRECTORY => [
            'original' => Brand::ORIGINAL_IMAGE_DISK,
            'resized'  => Brand::RESIZED_IMAGE_DISK,
        ],
        Category::IMAGE_DIRECTORY => [
            'original' => Category::ORIGINAL_IMAGE_DISK,
            'resized'  => Category::RESIZED_IMAGE_DISK,
        ],
    ];

    /**
     * @var Images
     */
    private $images;

    /**
     * @param Images $images
     */
    public function __construct(Images $images)
    {
        $this->images = $images;
    }

    /**
     * @param string $directory
     * @param string $filename
     * @param string $extension
     * @param int $width
     * @param int $height
     * @return mixed
     * @throws Exception
     */
    public function resize(string $directory, string $filename, int $width, int $height, string $extension)
    {
        if (!in_array($directory, array_keys(static::$directories))) {
            throw new Exception('Directory not found');
        }

        $disk = static::$directories[$directory];

        $newImage = $this->images->resize(
            $disk['original'],
            $disk['resized'],
            $directory,
            $filename,
            $extension,
            $width,
            $height
        );

        return $newImage->response();
    }
}
