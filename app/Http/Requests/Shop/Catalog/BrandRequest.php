<?php

namespace App\Http\Requests\Shop\Catalog;

use App\UseCase\Shop\Brand\ValidationRues;
use Illuminate\Foundation\Http\FormRequest;

class BrandRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return ValidationRues::resource($this->id ? (int) $this->id : null);
    }

    /**
     * @return array
     */
    public function validated(): array
    {
        $data = parent::validated();

        $data['image'] = $this->file('upload_image', $this->get('image'));

        return $data;
    }
}
