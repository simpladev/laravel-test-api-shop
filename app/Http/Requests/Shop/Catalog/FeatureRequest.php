<?php

namespace App\Http\Requests\Shop\Catalog;

use Illuminate\Foundation\Http\FormRequest;
use App\UseCase\Shop\Feature\ValidationRues;

class FeatureRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return ValidationRues::resource($this->id ? (int) $this->id : null);
    }
}
