<?php

namespace App\Http\Requests\Shop\Catalog;

use App\UseCase\Shop\Flag\ValidationRues;
use Illuminate\Foundation\Http\FormRequest;

class FlagRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return ValidationRues::resource($this->id ? (int) $this->id : null);
    }
}
