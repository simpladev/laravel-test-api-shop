<?php

namespace App\Http\Resources\Shop\Catalog\Brand;

use Illuminate\Http\Resources\Json\JsonResource;

class Base extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'name'    => $this->name,
            'preview' => $this->getResizedUrl(100, 100),
        ];
    }
}
