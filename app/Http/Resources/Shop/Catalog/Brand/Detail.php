<?php

namespace App\Http\Resources\Shop\Catalog\Brand;

use Illuminate\Http\Resources\Json\JsonResource;

class Detail extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'name'             => $this->name,
            'slug'             => $this->slug,
            'image'            => $this->image ?: '',
            'preview'          => $this->getResizedUrl(200, 200),
            'annotation'       => $this->annotation ?: '',
            'description'      => $this->description ?: '',
            'page_title'       => $this->page_title ?: '',
            'meta_title'       => $this->meta_title ?: '',
            'meta_keywords'    => $this->meta_keywords ?: '',
            'meta_description' => $this->meta_description ?: '',
        ];
    }
}
