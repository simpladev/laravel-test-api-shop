<?php

namespace App\Http\Resources\Shop\Catalog\Feature;

use Illuminate\Http\Resources\Json\JsonResource;

class Base extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'   => $this->id,
            'name' => $this->name,
            'code' => $this->code,
        ];
    }
}
