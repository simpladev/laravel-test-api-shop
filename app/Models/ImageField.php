<?php

namespace App\Models;

use App\Helpers\Images;

/**
 * @property string $image
 */
trait ImageField
{
    /**
     * @param int|null $width
     * @param int|null $height
     * @param bool $set_watermark
     * @return string
     */
    public function getResizedUrl(?int $width, ?int $height, bool $set_watermark = false): string
    {
        if (!$this->image) {
            return '';
        }

        /** @var Images $images */
        $images = app(Images::class);

        return $images->getResizedUrl(
            static::RESIZED_IMAGE_DISK,
            static::IMAGE_DIRECTORY,
            $this->image,
            $width,
            $height,
            $set_watermark
        );
    }
}
