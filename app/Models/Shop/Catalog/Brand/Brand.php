<?php

namespace App\Models\Shop\Catalog\Brand;

use App\Models\ImageField;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use ImageField;

    const ORIGINAL_IMAGE_DISK = 'local';
    const RESIZED_IMAGE_DISK  = 'public';
    const IMAGE_DIRECTORY     = 'shop/brands';

    protected $fillable = [
        'name',
        'slug',
        'image',
        'annotation',
        'description',
        'page_title',
        'meta_title',
        'meta_keywords',
        'meta_description',
    ];
}
