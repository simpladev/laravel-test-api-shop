<?php

namespace App\Models\Shop\Catalog\Brand;

use App\Helpers\Images;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;

class Observer
{
    /**
     * @var Images
     */
    private Images $images;

    /**
     * @param Images $images
     */
    public function __construct(Images $images)
    {
        $this->images = $images;
    }

    /**
     * @param Brand $brand
     */
    public function saving(Brand $brand)
    {
        $this->imageWatcher($brand);
        $this->slugWatcher($brand);
    }

    /**
     * @param Brand $brand
     */
    public function deleting(Brand $brand)
    {
        if ($brand->image) {
            $this->removeFile($brand->image);
        }
    }

    /**
     * @param Brand $brand
     */
    private function imageWatcher(Brand $brand): void
    {
        if (($oldImage = $brand->getOriginal('image')) && $oldImage !== $brand->image) {
            $this->removeFile($oldImage);
        }

        if ($brand->image instanceof UploadedFile) {
            $brand->image = $this->storeFile($brand->image);
        }
    }

    /**
     * @param Brand $brand
     */
    private function slugWatcher(Brand $brand)
    {
        if ($brand->getOriginal('slug')) {
            $brand->slug = Str::slug($brand->slug ?: $brand->name);
        }
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    private function storeFile(UploadedFile $file): string
    {
        $filename = Str::uuid()->toString() . '.' . $file->clientExtension();
        $file->storeAs(Brand::IMAGE_DIRECTORY, $filename, Brand::ORIGINAL_IMAGE_DISK);
        return $filename;
    }

    /**
     * @param string $file
     */
    private function removeFile(string $file)
    {
        $this->images->removeOriginal(Brand::ORIGINAL_IMAGE_DISK, Brand::IMAGE_DIRECTORY, $file);
        $this->images->removeResized(Brand::RESIZED_IMAGE_DISK, Brand::IMAGE_DIRECTORY, $file);
    }
}
