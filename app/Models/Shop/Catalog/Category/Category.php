<?php

namespace App\Models\Shop\Catalog\Category;

use App\Models\ImageField;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use ImageField;

    const ORIGINAL_IMAGE_DISK = 'local';
    const RESIZED_IMAGE_DISK  = 'public';
    const IMAGE_DIRECTORY     = 'shop/categories';

    protected $fillable = [
        'parent_id',
        'name',
        'slug',
        'image',
        'is_visible',
        'annotation',
        'description',
        'page_title',
        'meta_title',
        'meta_keywords',
        'meta_description',
    ];
}
