<?php

namespace App\Models\Shop\Catalog\Category;

use App\Helpers\Images;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;

class Observer
{
    /**
     * @var Images
     */
    private Images $images;

    /**
     * @param Images $images
     */
    public function __construct(Images $images)
    {
        $this->images = $images;
    }

    /**
     * @param Category $category
     */
    public function saving(Category $category)
    {
        $this->imageWatcher($category);
        $this->slugWatcher($category);
    }

    /**
     * @param Category $category
     */
    public function deleting(Category $category)
    {
        if ($category->image) {
            $this->removeFile($category->image);
        }
    }

    /**
     * @param Category $category
     */
    private function imageWatcher(Category $category): void
    {
        if (($oldImage = $category->getOriginal('image')) && $oldImage !== $category->image) {
            $this->removeFile($oldImage);
        }

        if ($category->image instanceof UploadedFile) {
            $category->image = $this->storeFile($category->image);
        }
    }

    /**
     * @param Category $category
     */
    private function slugWatcher(Category $category)
    {
        if ($category->getOriginal('slug')) {
            $category->slug = Str::slug($category->slug ?: $category->name);
        }
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    private function storeFile(UploadedFile $file): string
    {
        $filename = Str::uuid()->toString() . '.' . $file->clientExtension();
        $file->storeAs(Category::IMAGE_DIRECTORY, $filename, Category::ORIGINAL_IMAGE_DISK);
        return $filename;
    }

    /**
     * @param string $file
     */
    private function removeFile(string $file)
    {
        $this->images->removeOriginal(Category::ORIGINAL_IMAGE_DISK, Category::IMAGE_DIRECTORY, $file);
        $this->images->removeResized(Category::RESIZED_IMAGE_DISK, Category::IMAGE_DIRECTORY, $file);
    }
}
