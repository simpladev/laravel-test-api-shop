<?php

namespace App\Models\Shop\Catalog\Feature;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $fillable = [
        'name',
        'code',
    ];
}
