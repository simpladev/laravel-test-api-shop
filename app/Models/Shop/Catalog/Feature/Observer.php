<?php

namespace App\Models\Shop\Catalog\Feature;

use Illuminate\Support\Str;

class Observer
{
    /**
     * @param Feature $feature
     */
    public function saving(Feature $feature)
    {
        $this->codeWatcher($feature);
    }

    /**
     * @param Feature $feature
     */
    private function codeWatcher(Feature $feature)
    {
        if ($feature->getOriginal('code')) {
            $feature->code = Str::slug($feature->code ?: $feature->name);
        }
    }
}
