<?php

namespace App\Models\Shop\Catalog\Flag;

use Illuminate\Database\Eloquent\Model;

class Flag extends Model
{
    protected $fillable = [
        'name',
        'code',
        'color',
    ];
}
