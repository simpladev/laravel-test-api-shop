<?php

namespace App\Models\Shop\Catalog\Flag;

use Illuminate\Support\Str;

class Observer
{
    /**
     * @param Flag $flag
     */
    public function saving(Flag $flag)
    {
        $this->codeWatcher($flag);
    }

    /**
     * @param Flag $flag
     */
    private function codeWatcher(Flag $flag)
    {
        if ($flag->getOriginal('code')) {
            $flag->code = Str::slug($flag->code ?: $flag->name);
        }
    }
}
