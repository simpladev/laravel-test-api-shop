<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Shop\Catalog\Brand\Brand;
use App\Models\Shop\Catalog\Feature\Feature;
use App\Models\Shop\Catalog\Category\Category;
use App\Models\Shop\Catalog\Brand\Observer as BrandObserver;
use App\Models\Shop\Catalog\Feature\Observer as FeatureObserver;
use App\Models\Shop\Catalog\Category\Observer as CategoryObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Category::observe(CategoryObserver::class);
        Brand::observe(BrandObserver::class);
        Feature::observe(FeatureObserver::class);
    }
}
