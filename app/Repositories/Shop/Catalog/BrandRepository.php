<?php

namespace App\Repositories\Shop\Catalog;

use App\Models\Shop\Catalog\Brand\Brand;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class BrandRepository
{
    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return Brand::query();
    }

    /**
     * @param int $id
     * @param array $select
     * @return Brand
     */
    public function getOne(int $id, array $select = []): Brand
    {
        $query = $this->query();

        if ($select) {
            $query->select($select);
        }

        return $query->findOrFail($id);
    }

    /**
     * @param int $id
     * @return Brand|null
     */
    public function findOne(int $id): ?Brand
    {
        return $this->query()->find($id);
    }

    /**
     * @return Brand[]|Collection
     */
    public function getAll(): iterable
    {
        return $this->query()->get();
    }
}
