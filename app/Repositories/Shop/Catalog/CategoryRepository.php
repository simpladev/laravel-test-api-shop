<?php

namespace App\Repositories\Shop\Catalog;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Shop\Catalog\Category\Category;

class CategoryRepository
{
    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return Category::query();
    }

    /**
     * @param int $id
     * @param array $select
     * @return Category
     */
    public function getOne(int $id, array $select = []): Category
    {
        $query = $this->query();

        if ($select) {
            $query->select($select);
        }

        return $query->findOrFail($id);
    }

    /**
     * @param int $id
     * @return Category|null
     */
    public function findOne(int $id): ?Category
    {
        return $this->query()->find($id);
    }

    /**
     * @return Category[]|Collection
     */
    public function getAll(): iterable
    {
        return $this->query()->get();
    }
}
