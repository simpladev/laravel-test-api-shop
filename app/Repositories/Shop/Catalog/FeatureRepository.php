<?php

namespace App\Repositories\Shop\Catalog;

use Illuminate\Database\Eloquent\Builder;
use App\Models\Shop\Catalog\Feature\Feature;
use Illuminate\Database\Eloquent\Collection;

class FeatureRepository
{
    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return Feature::query();
    }

    /**
     * @param int $id
     * @param array $select
     * @return Feature
     */
    public function getOne(int $id, array $select = []): Feature
    {
        $query = $this->query();

        if ($select) {
            $query->select($select);
        }

        return $query->findOrFail($id);
    }

    /**
     * @param int $id
     * @return Feature|null
     */
    public function findOne(int $id): ?Feature
    {
        return $this->query()->find($id);
    }

    /**
     * @return Feature[]|Collection
     */
    public function getAll(): iterable
    {
        return $this->query()->get();
    }
}
