<?php

namespace App\Repositories\Shop\Catalog;

use App\Models\Shop\Catalog\Flag\Flag;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class FlagRepository
{
    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return Flag::query();
    }

    /**
     * @param int $id
     * @param array $select
     * @return Flag
     */
    public function getOne(int $id, array $select = []): Flag
    {
        $query = $this->query();

        if ($select) {
            $query->select($select);
        }

        return $query->findOrFail($id);
    }

    /**
     * @param int $id
     * @return Flag|null
     */
    public function findOne(int $id): ?Flag
    {
        return $this->query()->find($id);
    }

    /**
     * @return Flag[]|Collection
     */
    public function getAll(): iterable
    {
        return $this->query()->get();
    }
}
