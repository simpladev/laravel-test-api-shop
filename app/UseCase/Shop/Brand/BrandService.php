<?php

namespace App\UseCase\Shop\Brand;

use Exception;
use App\Models\Shop\Catalog\Brand\Brand;
use App\Repositories\Shop\Catalog\BrandRepository;

class BrandService
{
    /**
     * @var BrandRepository
     */
    private BrandRepository $brands;

    /**
     * @param BrandRepository $brands
     */
    public function __construct(BrandRepository $brands)
    {
        $this->brands = $brands;
    }

    /**
     * @param array $attributes
     * @return Brand
     */
    public function create(array $attributes): Brand
    {
        return Brand::create($attributes);
    }

    /**
     * @param int $id
     * @param array $attributes
     * @return Brand
     */
    public function update(int $id, array $attributes): Brand
    {
        $brand = $this->brands->getOne($id);

        $brand->update($attributes);

        return $brand;
    }

    /**
     * @param int $id
     * @throws Exception
     */
    public function delete(int $id)
    {
        $this->brands->getOne($id)->delete();
    }
}
