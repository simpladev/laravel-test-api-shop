<?php

namespace App\UseCase\Shop\Brand;

class ValidationRues
{
    /**
     * @param $id
     * @return array
     */
    public static function resource(int $id = null): array
    {
        return [
            'name'             => 'required|string',
            'slug'             => 'required|string|unique:brands,slug,' . $id,
            'annotation'       => 'nullable|string',
            'description'      => 'nullable|string',
            'page_title'       => 'nullable|string',
            'meta_title'       => 'nullable|string',
            'meta_keywords'    => 'nullable|string',
            'meta_description' => 'nullable|string',
            'image'            => 'nullable|string',
            'upload_image'     => 'nullable|image|mimes:jpg,jpeg,png',
        ];
    }
}
