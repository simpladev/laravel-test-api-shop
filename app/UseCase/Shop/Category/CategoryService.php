<?php

namespace App\UseCase\Shop\Category;

use Exception;
use App\Models\Shop\Catalog\Category\Category;
use App\Repositories\Shop\Catalog\CategoryRepository;

class CategoryService
{
    /**
     * @var CategoryRepository
     */
    private CategoryRepository $categories;

    /**
     * @param CategoryRepository $categories
     */
    public function __construct(CategoryRepository $categories)
    {
        $this->categories = $categories;
    }

    /**
     * @param array $attributes
     * @return Category
     */
    public function create(array $attributes): Category
    {
        return Category::create($attributes);
    }

    /**
     * @param int $id
     * @param array $attributes
     * @return Category
     */
    public function update(int $id, array $attributes): Category
    {
        $category = $this->categories->getOne($id);

        $category->update($attributes);

        return $category;
    }

    /**
     * @param int $id
     * @throws Exception
     */
    public function delete(int $id)
    {
        $this->categories->getOne($id)->delete();
    }
}
