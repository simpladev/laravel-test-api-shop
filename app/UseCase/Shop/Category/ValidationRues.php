<?php

namespace App\UseCase\Shop\Category;

use App\Helpers\Tables;

class ValidationRues
{
    /**
     * @param $id
     * @return array
     */
    public static function resource(int $id = null): array
    {
        return [
            'parent_id'        => 'nullable|integer|exists:categories,id',
            'name'             => 'required|string',
            'slug'             => 'required|string|unique:categories,slug,' . $id,
            'is_visible'       => 'boolean',
            'annotation'       => 'nullable|string',
            'description'      => 'nullable|string',
            'page_title'       => 'nullable|string',
            'meta_title'       => 'nullable|string',
            'meta_keywords'    => 'nullable|string',
            'meta_description' => 'nullable|string',
            'image'            => 'nullable|string',
            'upload_image'     => 'nullable|image|mimes:jpg,jpeg,png',
        ];
    }
}
