<?php

namespace App\UseCase\Shop\Feature;

use Exception;
use App\Models\Shop\Catalog\Feature\Feature;
use App\Repositories\Shop\Catalog\FeatureRepository;

class FeatureService
{
    /**
     * @var FeatureRepository
     */
    private FeatureRepository $features;

    /**
     * @param FeatureRepository $features
     */
    public function __construct(FeatureRepository $features)
    {
        $this->features = $features;
    }

    /**
     * @param array $attributes
     * @return Feature
     */
    public function create(array $attributes): Feature
    {
        return Feature::create($attributes);
    }

    /**
     * @param int $id
     * @param array $attributes
     * @return Feature
     */
    public function update(int $id, array $attributes): Feature
    {
        $feature = $this->features->getOne($id);

        $feature->update($attributes);

        return $feature;
    }

    /**
     * @param int $id
     * @throws Exception
     */
    public function delete(int $id)
    {
        $this->features->getOne($id)->delete();
    }
}
