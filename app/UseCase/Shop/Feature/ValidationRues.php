<?php

namespace App\UseCase\Shop\Feature;

class ValidationRues
{
    /**
     * @param int|null $id
     * @return array
     */
    public static function resource(int $id = null): array
    {
        return [
            'name' => 'required|string',
            'code' => 'required|string|unique:features,code,' . $id,
        ];
    }
}
