<?php

namespace App\UseCase\Shop\Flag;

use Exception;
use App\Models\Shop\Catalog\Flag\Flag;
use App\Repositories\Shop\Catalog\FlagRepository;

class FlagService
{
    /**
     * @var FlagRepository
     */
    private FlagRepository $flags;

    /**
     * @param FlagRepository $flags
     */
    public function __construct(FlagRepository $flags)
    {
        $this->flags = $flags;
    }

    /**
     * @param array $attributes
     * @return Flag
     */
    public function create(array $attributes): Flag
    {
        return Flag::create($attributes);
    }

    /**
     * @param int $id
     * @param array $attributes
     * @return Flag
     */
    public function update(int $id, array $attributes): Flag
    {
        $flag = $this->flags->getOne($id);

        $flag->update($attributes);

        return $flag;
    }

    /**
     * @param int $id
     * @throws Exception
     */
    public function delete(int $id)
    {
        $this->flags->getOne($id)->delete();
    }
}
