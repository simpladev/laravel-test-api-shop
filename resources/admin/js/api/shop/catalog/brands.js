export function getBrandById(id) {
    return axios.get(`/api/shop/catalog/brands/${id}`)
}

export function getBrands(params) {
    return axios.get(`/api/shop/catalog/brands`, {params})
}

export function addBrand(data) {
    return axios.post('/api/shop/catalog/brands', data)
}

export function updateBrand(id, data) {
    data.append("_method", "put");
    return axios.post(`/api/shop/catalog/brands/${id}`, data)
}

export function deleteBrand(id) {
    return axios.delete('/api/shop/catalog/brands/' + id)
}
