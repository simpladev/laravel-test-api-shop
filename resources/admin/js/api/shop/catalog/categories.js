export function getCategoryById(id) {
    return axios.get(`/api/shop/catalog/categories/${id}`)
}

export function getCategories(params) {
    return axios.get(`/api/shop/catalog/categories`, {params})
}

export function addCategory(data) {
    return axios.post('/api/shop/catalog/categories', data)
}

export function updateCategory(id, data) {
    data.append("_method", "put");
    return axios.post(`/api/shop/catalog/categories/${id}`, data)
}

export function deleteCategory(id) {
    return axios.delete('/api/shop/catalog/categories/' + id)
}
