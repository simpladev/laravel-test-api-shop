export function getFeatureById(id) {
    return axios.get(`/api/shop/catalog/features/${id}`)
}

export function getFeatures(params) {
    return axios.get(`/api/shop/catalog/features`, {params})
}

export function addFeature(data) {
    return axios.post('/api/shop/catalog/features', data)
}

export function updateFeature(id, data) {
    return axios.put(`/api/shop/catalog/features/${id}`, data)
}

export function deleteFeature(id) {
    return axios.delete('/api/shop/catalog/features/' + id)
}
