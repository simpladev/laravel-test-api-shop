export function getFlagById(id) {
    return axios.get(`/api/shop/catalog/flags/${id}`)
}

export function getFlags(params) {
    return axios.get(`/api/shop/catalog/flags`, {params})
}

export function addFlag(data) {
    return axios.post('/api/shop/catalog/flags', data)
}

export function updateFlag(id, data) {
    return axios.put(`/api/shop/catalog/flags/${id}`, data)
}

export function deleteFlag(id) {
    return axios.delete('/api/shop/catalog/flags/' + id)
}
