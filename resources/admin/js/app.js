// Vendor
require('./bootstrap');
require('select2');
require('./common');

// Vue
import Vue from 'vue';
window.Vue = require('vue');

// Components
Vue.component('vue-layouts-nav', require('./components/layouts/NavComponent').default);
Vue.component('vue-pagination', require('./components/parts/Pagination').default);
Vue.component('FlashMessage', require('./components/parts/FlashMessage').default);

// Plugins
import flash from "./plugins/flash";
Vue.use(flash);

// App
import router from './router';
import store from './store';

const app = new Vue({
    router,
    store
}).$mount('#app');
