$(document).ready(function () {
    if ($.fn.tooltip && $('[data-toggle="tooltip"]').length) {
        $('[data-toggle="tooltip"]').tooltip().on("mouseleave", function () {
            $(this).tooltip('hide');
        });
    }
});

if ($.fn.select2) {
    $.fn.select2.defaults.set("theme", "bootstrap");
    $.fn.select2.defaults.set("language", "ru");
}
