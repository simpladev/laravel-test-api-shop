export function appendFieldToCollection(objects, field, value) {
    objects.forEach(o => {
        appendField(o, field, value);
    });
    return objects;
}

export function appendField(object, field, value) {
    object[field] = value;
    return object;
}

/**
 * @param {Object} object
 * @returns {FormData}
 */
export function objectToFormData(object) {
    let data = new FormData();

    for (let key in object) {
        data.append(key, object[key]);
    }

    return data;
}
