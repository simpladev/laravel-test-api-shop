export function isRoutePath(name) {
    return this.$route.name.indexOf(name) === 0;
}
