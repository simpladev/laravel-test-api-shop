import store from '../store'

const flash = {
    install(Vue, options) {
        Vue.prototype.$flash = new Vue({
            methods: {
                success(text) {
                    store.commit('addFlashMessage', {type: 'success', text: text});
                },
                error(text) {
                    store.commit('addFlashMessage', {type: 'error', text: text});
                }
            }
        })
    }
};

export default flash
