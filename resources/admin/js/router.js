import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: require('./components/HomeComponent').default
        },
        {
            path: '/shop/catalog',
            name: 'shop.catalog.',
            component: require('./components/shop/catalog/Catalog').default,
            children:[
                // Categories
                {
                    path: 'categories',
                    name: 'shop.catalog.categories.index',
                    component: require('./components/shop/catalog/categories/Index').default
                },
                {
                    path: 'categories/create',
                    name: 'shop.catalog.categories.create',
                    component: require('./components/shop/catalog/categories/Form').default
                },
                {
                    path: 'categories/:id',
                    name: 'shop.catalog.categories.edit',
                    component: require('./components/shop/catalog/categories/Form').default
                },

                // Brands
                {
                    path: 'brands',
                    name: 'shop.catalog.brands.index',
                    component: require('./components/shop/catalog/brands/Index').default
                },
                {
                    path: 'brands/create',
                    name: 'shop.catalog.brands.create',
                    component: require('./components/shop/catalog/brands/Form').default
                },
                {
                    path: 'brands/:id',
                    name: 'shop.catalog.brands.edit',
                    component: require('./components/shop/catalog/brands/Form').default
                },

                // Features
                {
                    path: 'features',
                    name: 'shop.catalog.features.index',
                    component: require('./components/shop/catalog/features/Index').default
                },
                {
                    path: 'features/create',
                    name: 'shop.catalog.features.create',
                    component: require('./components/shop/catalog/features/Form').default
                },
                {
                    path: 'features/:id',
                    name: 'shop.catalog.features.edit',
                    component: require('./components/shop/catalog/features/Form').default
                },

                // Flags
                {
                    path: 'flags',
                    name: 'shop.catalog.flags.index',
                    component: require('./components/shop/catalog/flags/Index').default
                },
                {
                    path: 'flags/create',
                    name: 'shop.catalog.flags.create',
                    component: require('./components/shop/catalog/flags/Form').default
                },
                {
                    path: 'flags/:id',
                    name: 'shop.catalog.flags.edit',
                    component: require('./components/shop/catalog/flags/Form').default
                },
            ]
        },
    ]
});

router.beforeEach((to, from, next) => {
    next();
});

router.afterEach((to, from) => {

});

export default router;
