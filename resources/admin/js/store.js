import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        flashMessages: [],
    },
    mutations: {
        // FlashMessages
        addFlashMessage(state, message) {
            state.flashMessages.push(message);

            setTimeout(() => {
                state.flashMessages.splice(0, 1);
            }, 5000);
        }
    }
});
