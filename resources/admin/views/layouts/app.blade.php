<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js', 'build/admin') }}" defer></script>

    <!-- Styles -->
    <link href="{{ mix('css/app.css', 'build/admin') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <flash-message></flash-message>
    <vue-layouts-nav></vue-layouts-nav>

    <main>
        <router-view></router-view>
    </main>
</div>

@stack('scripts-footer')
</body>
</html>
