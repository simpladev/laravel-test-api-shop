@extends('layouts.app')

@section('content')
    @yield('header')
    <div class="container py-4">
        <div class="row">
            <div class="col-md-12">
                @yield('wrap-content')
            </div>
        </div>
    </div>
@endsection
